﻿--Fournisseur

MERGE INTO EmailCheck AS E
USING (SELECT * FROM Transaction_Link_Provider) T 
 ON E.Lien = T.Lien AND E.NoCie = T.NoCie
	WHEN NOT MATCHED 
	    THEN INSERT (Date_init,Date_Echeance,NoCie,Lien,Frequence,is_complete,Id_Destinataire,Section) VALUES (T.Date_Creation,T.Date_Limite,T.NoCie,T.Lien,0,0,T.NoCie,1)
    WHEN MATCHED
         THEN UPDATE SET E.is_complete = T.is_complete;

--Magasin

MERGE INTO EmailCheck AS E
USING (SELECT * FROM Transaction_Store) T 
 ON E.Lien = T.Lien AND E.Id_Destinataire = T.NoMag
	WHEN NOT MATCHED  
	    THEN INSERT (Date_init,Date_Echeance,NoCie,Lien,Frequence,is_complete,Id_Destinataire,Section) VALUES (T.Date_Envoi,T.Date_Limite,T.IdCie,T.Lien,0,0,T.NoMag,3)
    WHEN MATCHED
         THEN UPDATE SET E.is_complete = T.is_complete;  