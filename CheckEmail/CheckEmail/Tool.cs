﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Text.RegularExpressions;
using RetourArchambault;
using System.Threading;
using System.Globalization;

/// <summary>
/// Description résumée de Tool
/// </summary>
public static class Tool
{
	 
   
    /// <summary>
    /// Création du nom de fichier
    /// </summary>
    /// <param name="amem">Numero de AMEM(No employé qui crée la demande</param>
    /// <param name="provider">Numéro du fournisseur</param>
    /// <param name="dept">Numero du département</param>
    /// <param name="index">1=nouveau 2=Retour 3=Store 4-Historique</param>
    /// <returns></returns>
    public static string createNameFile(string amem,string provider,string dept,int index)
    {
        string fileName ="";
        fileName = Tool.format_Nombre(provider, 6) + Tool.format_Nombre(amem, 6) + dept + DateTime.Now.ToString("yyMMdd"); //+ "_" + index;
        fileName = fileName.Replace(":", "");
        fileName = fileName.Replace("-", "");
        fileName = fileName.Replace(" ", "");
        return fileName;
    }
    /// <summary>
    /// Formater le numéro selon le nombre de zéro désiré
    /// </summary>
    /// <param name="text">Nombre initial</param>
    /// <param name="nbr_caracteres">Nombre désiré</param>
    /// <returns>Nouveau nombre formaté</returns>
    public static string format_Nombre(string text, int nbr_caracteres)
    {
        string m_response = null;
        text = format_symbol(text);
        int generation = Convert.ToInt32(text);
        m_response = String.Format("{0:D" + nbr_caracteres + "}", generation);
        return m_response;
    }
    /// <summary>
    /// Supprimer les caractère autre que des nombres
    /// </summary>
    /// <param name="nombre"></param>
    /// <returns></returns>
    public static string format_symbol(string nombre)
    {
        // Remplace les caractères
        try
        {
            return Regex.Replace(nombre, @"[^\w\.@-]", "");

        }
        // If we timeout when replacing invalid characters, 
        // we should return Empty.
        catch (Exception)
        {
            return String.Empty;
        }
    }
    /// <summary>
    /// Obtient le nom du fichier sans l'extension
    /// </summary>
    /// <param name="longName">Nom complet du fichier avec path</param>
    /// <returns>Nom du fichier sans path et extension</returns>
    public static string shortName(string longName)
    {
        string rep = "";

        rep = longName.Substring(longName.LastIndexOf("\\") + 1);
        rep = rep.Substring(0, rep.Length - 4);
        return rep;
    }
    /// <summary>
    /// Extrait la date du nom de fichier
    /// </summary>
    /// <param name="LaDate">Date de 6 caractere</param>
    /// <returns>La date séparé par des /</returns>
    public static string GetDate(string maDate)
    {
        maDate = maDate.Insert(2, "/");
        maDate = maDate.Insert(5, "/");
        return maDate;
    
    }
  
    /// <summary>
    /// Recherche 0-NoFournisseur 1-NoAmem 2-Departement 3-Date 4-Etape
    /// </summary>
    /// <param name="lien">Chemin du fichier sans extension</param>
    /// <returns>Tableau descriptif</returns>
    public static string[] ExtractLien(string lien)
    {
        string[] Lien = new string[5];
        Lien[0] = lien.Substring(0, 6);
        Lien[1] = lien.Substring(6, 6);
        Lien[2] = lien.Substring(12, 3);
        Lien[3] = lien.Substring(15, 6);
        Lien[4] =lien.Substring((lien.Length - 1), 1);
        return Lien;
    }
   public static String GetTimeStamp()
    {
        return DateTime.Now.ToString("yyyyMMddHHmmssffff");

    }
   public static string getQuery(string queryName)
   {
       string query = string.Empty;
       query = File.ReadAllText("Requetes/" + queryName);
       return query;
   }
   public static void ErrorMail(string errorMessage)
   {
     //  EnvoiEmail em = new EnvoiEmail(1, 2, 5, false, errorMessage,DateTime.Now.ToShortDateString());

   }
    public static string formatCondition(string condition)
    {
        condition = condition.Replace("'", "+CHAR(39)+");
        return condition;
    }
    public static void setThread_EN()
    {
        if (Thread.CurrentThread.CurrentCulture.Name != "en-us")
        {
            //Set culture info
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-us");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-us");
            
        }
    }
    public static void setThread_FR()
    {
        if (Thread.CurrentThread.CurrentCulture.Name != "fr-CA")
        {
            //Set culture info
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-CA");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("fr-CA");

        }
    }
    public static DateTime setDate(int nbrJour)
    {
        DateTime d = DateTime.Now;
       
            d = d.AddDays(nbrJour);

            return d.Date;
    }

    
}