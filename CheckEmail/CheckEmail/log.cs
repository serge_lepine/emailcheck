﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utilitaires;
using System.Configuration;
/// <summary>
/// Description résumée de log
/// </summary>
public class log
{
   
    /// <summary>
    /// Ecrire dans journal Erreur
    /// </summary>
    /// <param name="message">Message d'erreur à écrire</param>
	public static void logError(string message)
	{
        if (ConfigurationSettings.AppSettings["logError"] == "1")
        {
            string buffer = "\r\n=======================================================================================";
            string path = String.Format("log/{0}_error.log", DateTime.Now.ToShortDateString(), "aa-mm-jj");
            IniFile.EcrireFichier(path, DateTime.Now.ToLongTimeString() + " : " + message + buffer);
        }
        //Avise le support par courriel
        Tool.ErrorMail(message);
        
	}
    /// <summary>
    /// Écrire dans journal log
    /// </summary>
    /// <param name="message">Message à inscrire</param>
    public static void Log(string message)
    {
        if (ConfigurationSettings.AppSettings["log"] == "1")
        {
            string buffer = "\r\n=======================================================================================";
            string path = String.Format("log/{0}_log.log", DateTime.Now.ToShortDateString(), "aa-mm-jj");
            IniFile.EcrireFichier(path, DateTime.Now.ToLongTimeString() + " : " + message + buffer);
        }
    }
}