﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace RetourArchambault
{
    public class EnvoiEmail
    {
        private string txtFrom = ConfigurationSettings.AppSettings["mailFrom"];
        private string txtSubject = "";
        private string message = "";
        private string pathSubject = "";
        private string pathConfirm = "";
        private Courriel courriel;
        private MailMessage mailobj;
        SmtpClient SMTPServer;
        MailAddress expediteur;
        private int id_destinataire;
        private int Section;
        private int id_Provider;
        private DateTime dateEnvoi;
        private int frequence;
        private int Id;

        public EnvoiEmail()
        {

        }
        /// <summary>
        /// Instancie EnvoiEmail
        /// </summary>
        /// <param name="param"></param>
        public EnvoiEmail(SendTo param)
        {
            //Creer update avec la frequence
            this.frequence = param.Frequence;
            
            this.id_Provider = param.Id_provider;
            this.Section = param.section;
            this.id_destinataire = param.Id_destinataire;
            this.dateEnvoi = param.DateEnvoi;
            this.Id = param.Id;
           

          //  setEmail();
       

            ////Joindre le message si erreur
            //if (!param.succes)
            //{
            //    this.txtSubject = String.Format(txtSubject, this.id_Provider.ToString(), this.courriel.cieName);
            //    this.message = String.Format(message, param.errorMessage);
            //}

           // sendMail();

        }
        /// <summary>
        ///  Instancie les variable d'envoi
        /// </summary>
        public void setEmail()
        {
            //Obtenir la liste des courriel et le nom du fournisseur
            this.courriel = Connexion.getEmail(this.id_destinataire, this.id_Provider);
            this.pathConfirm = String.Format("Rappel_Section_{0}.txt", this.Section);
            this.pathSubject = String.Format("Rappel_Subject_{0}.txt", this.Section);
            this.txtSubject = File.ReadAllText(String.Format("Email/" + this.pathSubject));
            this.txtSubject = String.Format(txtSubject, this.id_Provider.ToString(), this.courriel.cieName,this.dateEnvoi);
            this.message = File.ReadAllText(String.Format("Email/" + this.pathConfirm));
        }
        /// <summary>
        /// Envoi courriel
        /// </summary>
        public bool sendMail()
        {
           
            this.mailobj = new MailMessage();
            try
            {
                foreach (var adress in this.courriel.emailAdress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    this.mailobj.To.Add(adress);
                }

                this.expediteur = new MailAddress(this.txtFrom);
                this.mailobj.Body = this.message;
                this.mailobj.Subject = this.txtSubject;
                this.mailobj.From = this.expediteur;
                this.SMTPServer = new SmtpClient();
                this.SMTPServer.UseDefaultCredentials = false;
                this.SMTPServer.Host = "courriel.archambault.ca";
                this.SMTPServer.Port = 25;
                this.SMTPServer.EnableSsl = false;


                this.SMTPServer.Send(mailobj);

                //Conserver une trace des email dans BD
                log.Log("Courriel succes à : " + this.id_destinataire.ToString());
                return true;

            }
            catch (Exception ex)
            {
                log.logError("Problème envoi courriel : " + ex.ToString());
                return false;
            }
        }
        /// <summary>
        ///  Met à jour la frequence et la date echeance
        /// </summary>
        public void updateFrequence()
        {
            int jour = 0;
            if(this.frequence == 0){jour = 7;}else{jour = 1;}
            string query = Tool.getQuery("setFrequence.sql");
            query = String.Format(query,1,"GETDATE() + " + jour,this.Id);
            log.Log(query);
            if(!Connexion.execCommand(query))
            {
                log.logError("Impossible de mettre à jour");
                Console.WriteLine("Erreur lors de la mise à jour EmailCheck");
            }
        }

   
    }

}