﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace RetourArchambault
{
    public class Courriel
    {
        
        public string cieName { get; set; }
        public string emailAdress { get; set; }
    
        public Courriel()
        {

        }
       
 
    }
    public class SendTo
    {
        public int Id_provider{get;set;}
        public int Id_destinataire {get;set;}
        public int section {get;set;}
        public bool succes {get;set;}
        public string errorMessage {get;set;}
        public DateTime DateEnvoi {get;set;}
        public int Frequence { get; set; }
        public int Id { get; set; }

        public SendTo()
        {

        }
    }
}