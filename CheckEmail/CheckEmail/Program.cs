﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using RetourArchambault;

namespace CheckEmail
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1 - Mettre à jour table EmailCheck
            string query = Tool.getQuery("setEmailList.sql");
            if( Connexion.execCommand(query))
            {
                // 2 - Rechercher si échéance
                string searchTerm = Tool.getQuery("getEmailList.sql");
               
                if( Connexion.execScalar(searchTerm))
                {
                    SqlConnection con = Connexion.ConnectSQL();
                    try
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            SqlCommand cmd = new SqlCommand(searchTerm, con);
                            SqlDataReader reader = cmd.ExecuteReader();

                            while (reader.Read())
                            {
                                SendTo st = new SendTo();
                                st.Id = int.Parse(reader[0].ToString());
                                st.Id_provider = int.Parse(reader[1].ToString());
                                st.Id_destinataire = int.Parse(reader[2].ToString());
                                st.section = int.Parse(reader[3].ToString());
                                st.DateEnvoi = DateTime.Parse(reader[4].ToString());
                                st.Frequence = int.Parse(reader[5].ToString());
                                //Envoi courriel
                                EnvoiEmail em = new EnvoiEmail(st);
                                em.setEmail();
                                if (em.sendMail())
                                {
                                    em.updateFrequence();
                                }
                                else
                                {
                                    log.logError(" Probleme envoiEmail");
                                }


                            }


                        }
                    }
                    catch (Exception ex)
                    {
                        log.logError("Erreur lors de la recherche Term : " + ex.Message + " StackTrace : " + ex.StackTrace);
                    }
                    finally
                    {
                        Connexion.closeSQL(con);
                    }
                   
                }
              

                
            }
           
        }
    }
}
