﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.Threading;
using RetourArchambault;
/// <summary>
/// Description résumée de Connexion
/// </summary>
public class Connexion
{
    
	public static OleDbConnection Connect()
	{
        
        OleDbConnection con = null;
        if (ConfigurationSettings.AppSettings["DEV"] == "1")
        {
            con = new OleDbConnection(ConfigurationSettings.AppSettings["conn400DEV"]);
        }
        else
        {
            con = new OleDbConnection(ConfigurationSettings.AppSettings["conn400PRD"]);
        }
        
       
        try
        {
            con.Open();
            
        }
        catch (OleDbException oe)
        {
            log.logError(oe.Message);
        }
        return con;
	}
    public static SqlConnection ConnectSQL()
    {
        SqlConnection conSQL = null;
        try 
        {
            conSQL = new SqlConnection(ConfigurationSettings.AppSettings["connSQL"]);
            conSQL.Open();
        }
        catch (Exception o)
        {

            log.logError(o.Message);
        }
        return conSQL;

    }
    public static void close(OleDbConnection con)
    {
        if (con.State != ConnectionState.Closed)
        {
            con.Close();
            con.Dispose();
        }
    
    }
    public static void closeSQL(SqlConnection con)
    {
        if (con.State != ConnectionState.Closed)
        {
            con.Close();
            con.Dispose();
        }

    }
    public static bool execCommand(string query)
    {
        SqlConnection con = ConnectSQL();

        SqlCommand cmd = new SqlCommand(query, con);
      
        try
        {
            cmd.ExecuteNonQuery();
        }
        catch (SqlException se)
        {
            log.logError("SqlException erreur " + se.Message);
            return false;

        }
        catch (Exception e)
        {
            log.logError("Connexion erreur " + e.Message);
            return false;

        }
        
        finally
        {
            cmd.Dispose();
            Connexion.closeSQL(con);
          
        }

        return true;
    }
    public static bool execScalar(string query)
    {
        SqlConnection con = ConnectSQL();
        log.Log(query);
        SqlCommand cmd = new SqlCommand(query, con);

        try
        {
           int rep = Convert.ToInt32(cmd.ExecuteScalar());
            if (rep == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        catch (SqlException se)
        {
            log.logError("SqlException erreur " + se.Message);
            return false;

        }
        catch (Exception e)
        {
            log.logError("Connexion erreur " + e.Message);
            return false;

        }

        finally
        {
            cmd.Dispose();
            Connexion.closeSQL(con);

        }

      //  return true;
    }
    /// <summary>
    /// Obtient la liste adresse de courriel et le nom du fournisseur
    /// </summary>
    /// <param name="Id_Destinataire"></param>
    /// <param name="id_Provider"></param>
    /// <returns></returns>
    public static Courriel getEmail(int Id_Destinataire,int id_Provider)
    {

        Courriel cr = new Courriel();
        SqlConnection con = ConnectSQL();
        SqlCommand cmd = new SqlCommand();
        string query = Tool.getQuery("sendEmail.sql");
        query = String.Format(query, Id_Destinataire,id_Provider);
        cmd.CommandText = query;
        cmd.Connection = con;
        SqlDataReader reader;
        int i = 0;
        try 
        {
            reader = cmd.ExecuteReader();
            //Stokc les adresses email du destinataire
                while (reader.Read())
                {
                    cr.emailAdress += reader[0] + ";"; 
                }
             
            reader.NextResult();
            reader.Read();
            //Recherche le nom du fournisseur
            if (reader.HasRows)
            {
                cr.cieName = reader[0].ToString();
            }
            reader.Close();
        }
        catch (SqlException sqlEx)
        {
            log.logError("getEmail : " + sqlEx.Message);
         
            
        }
        catch (Exception ex)
        {
            log.logError("Connexion : " + ex.Message);
        }
        finally
        {
            cmd.Dispose();
            Connexion.closeSQL(con);
        }
        return cr;
    }
    public static int getSection(string Lien)
    {
        int rep = 0;
        string query = Tool.getQuery("getSection.sql");
        query = string.Format(query, Lien);

        SqlConnection con = ConnectSQL();
        SqlCommand cmd = new SqlCommand(query, con);
        SqlDataReader reader;

        try
        {
            reader = cmd.ExecuteReader();
            reader.Read();
            rep = Convert.ToInt32(reader[0].ToString());
        }
        catch (Exception ex)
        {
            log.logError("Connexion.getSection erreur : " + ex.Message);
            rep = 0;
        }
        finally
        {
            cmd.Dispose();
            closeSQL(con);

        }
        return rep;
    }
    //public static ConditionReponse getCondition(string Lien)
    //{
    //    ConditionReponse rep = new ConditionReponse();
    //    string query = Tool.getQuery("AmemConditionCheck.sql");
    //    query = string.Format(query, Lien);

    //    SqlConnection con = ConnectSQL();
    //    SqlCommand cmd = new SqlCommand(query, con);
    //    SqlDataReader reader;

    //    try
    //    {
    //        reader = cmd.ExecuteReader();
    //        reader.Read();
    //        rep.condition = reader[0].ToString();
    //        if(!String.IsNullOrEmpty(reader[1].ToString())){rep.qty_minus = int.Parse(reader[1].ToString());}else{rep.qty_minus = 0;} 
    //        if(!String.IsNullOrEmpty(reader[2].ToString())){rep.qty_over = int.Parse(reader[2].ToString());}else{rep.qty_over = 0;}
    //        if(!String.IsNullOrEmpty(reader[3].ToString())){rep.model = int.Parse(reader[3].ToString());}else{rep.model = 0;}
    //    }
    //    catch (Exception ex)
    //    {
    //        log.logError("Connexion.getCondition erreur : " + ex.Message);
    //        rep = null;
    //    }
    //    finally
    //    {
    //        cmd.Dispose();
    //        closeSQL(con);

    //    }
    //    return rep;
    //}
    public static bool getAutorise(string lien)
    {
        string query = Tool.getQuery("AutorisationVerif.sql");
        query = String.Format(query, lien);
        SqlConnection con = ConnectSQL();
        SqlCommand cmd = new SqlCommand(query, con);
        SqlDataReader reader;
        int rep = 1;
        try
        {
            reader = cmd.ExecuteReader();
            reader.Read();

            if (!String.IsNullOrEmpty(reader[0].ToString())) { rep = int.Parse(reader[0].ToString()); } else { rep = 1; }
            
        }
        catch (Exception ex)
        {
            log.logError("Connexion.getCondition erreur : " + ex.Message);
           
        }
        finally
        {
            cmd.Dispose();
            closeSQL(con);

        }
        if(rep == 0)
        {
            //Si 0 alors tous les nautorisation sont complété
            //Retourne vrai
            return true;
        }
       else
        {
            //Sinon retourne faux
            return false;
        }

    }
}